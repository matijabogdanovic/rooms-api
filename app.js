// app.js

var express    = require('express');
var app        = express();
var bodyParser = require('body-parser');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

require('./routes')(app);

var port = process.env.PORT || 8080;
app.listen(port);
console.log('server port: ' + port);