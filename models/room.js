//room.js

module.exports = (function (id, name, count) {

	this.id = id;
	this.name = name;
	this.count = count;

	return {
		id:this.id,
		name: this.name,
		count: this.count
	}

});