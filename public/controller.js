//controller.js

(function(){
	var list = $("#roomsList");
	var baseUrl = '/room';
	readRooms();
	$("#submit").click(createRoom);

	function createRoom() {
		var inputData = $('#inputForm').serialize();
		var url = baseUrl + 's/';
		httpRequest(url, 'POST', insertRoomToList, inputData);
	}

	function deleteRoom(event) {
		var id = event.data.id;
		var url = baseUrl + '/' + id;
		httpRequest(url, 'DELETE', removeRoomFromList);
	}

	//--------------------------------------------------------------------------

	function readRooms() {
		var url = baseUrl + 's/';
		httpRequest(url, 'GET', insertRoomsToList);
	}

	//--------------------------------------------------------------------------

	function updateRoom(event) {
		var url = '/room/' + event.data.id;
		httpRequest(url, 'PATCH', updateRoomInList);
	}

	//--------------------------------------------------------------------------

	function httpRequest(url, type, success, data) {
		$.ajax({
			url: url,
			type: type,
			success: success,
			data:data
		});
	}

	//==========================================================================

	function appendDeleteButtonToLi(room, li) {
		var btn = createButton('Delete ', room);
		btn.on('click',{id:room.id}, deleteRoom);
		(li).append(btn);
	}

	//--------------------------------------------------------------------------

	function appendUpdateButtonToLi(room, li) {
		var btn = createButton('Update ', room);
		btn.on('click',{id:room.id}, updateRoom);
		(li).append(btn);
	}

	//--------------------------------------------------------------------------

	function createButton(label, room) {
		var str = label + room.id;
		var btn = $('<button/>', {text:str});
		return btn;
	}

	//--------------------------------------------------------------------------

	function createRoomLi(room) {
		var li = $('<li/>', {id:'room'+room.id});
		var str = roomToStr(room);
		var p = $('<p/>', {text:str}).appendTo(li);
		appendDeleteButtonToLi(room, li);
		appendUpdateButtonToLi(room, li);
		return li;
	}

	//--------------------------------------------------------------------------

	function insertRoomToList(room) {
		var li = createRoomLi(room);
		list.append(li);
	}

	//--------------------------------------------------------------------------

	function insertRoomsToList(rooms) {
		list.empty();
		for (i in rooms) {
			var li = createRoomLi(rooms[i]);
			list.append(li);
		}
	}

	//--------------------------------------------------------------------------

	function removeRoomFromList(room) {
		var roomId = '#room' + room.id;
		$(roomId).remove()
	}

	//--------------------------------------------------------------------------

	function roomToStr(room) {
		return 'name:' + room.name + ', ' + 'count:' + room.count ;
	}
	//--------------------------------------------------------------------------

	function updateRoomInList(room) {
		var roomId = '#room' + room.id + ' p';
		var str = roomToStr(room);
		$(roomId).text(str);
	}

	//--------------------------------------------------------------------------

})();