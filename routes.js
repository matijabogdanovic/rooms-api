//routes.js

module.exports = function(app){  
    
    //--------------------------------------------------------------------------
    
    var express = require('express');
    var router = express.Router();
    app.use(express.static('public'));
    app.use('/', router);

    //--------------------------------------------------------------------------

    var Room = require('./models/room');
    rooms = {0:new Room(0, 's1', 100), 1:new Room(1, 's2', 200)};
    var id = Object.keys(rooms).length;
    
    //--------------------------------------------------------------------------

    router.route('/rooms')
    .get(function(req, res) {
        res.json(rooms);
    })
    .post(function(req, res) {
        var count = req.body.count? parseInt(req.body.count) : 0;
        var name = req.body.name;
        var room = new Room(id, name, count);
        rooms[id++] = room;
        res.json(room);
    });    

    //--------------------------------------------------------------------------

    router.route('/room/:id')
    .delete(function(req, res) {
        var id = req.params.id;
        var room = rooms[id];
        delete rooms[id];
        return res.json(room);
    })
    .get(function(req, res) {
        var id = req.params.id
        return res.json(rooms[id])
    })
    .patch(function(req, res) {
        var id = req.params.id;
        var room = rooms[id];
        room.count++;
        return res.json(room);
    });

    //--------------------------------------------------------------------------
}